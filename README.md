[latest built pdf](https://gitlab.com/papagandalf/gitlab_latex_ci/-/jobs/artifacts/master/browse?job=build)

# gitlab-latex-ci

A basic ```.gitlab-ci.yml``` useful for building latex files using Gitlab's CI. Feel free to use it for your projects!

## what it does
Every time you push, a pdf of your latex project is built. 

Moreover, to help ~~students~~ people track (in a nice way) changes ~~their supervisors~~ collaborators make without leaving comments, a pdf version with the difference between the latest two versions is generated.

Both the final pdf and the "change-tracking" file can be found in the artifact section of CI/CD jobs.

## how to use
File ```.gitlab-ci.yml``` is relatively self-explanatory.
You may need to change the FILE_NAME to point to your (main) ```.tex``` file (just the filename without the extension).
In the ```artifacts``` section, you specify the files you need to be availble to you after the build.

## notes
* You can specify a specific branch that triggers the build. In general, if you are not familiar with Gitlab CI, it is really worth taking a look at the [documentation pages](https://about.gitlab.com/features/gitlab-ci-cd/).
* Keep in mind that you get [2,000 CI pipeline minutes per month on shared runners](https://about.gitlab.com/gitlab-com/) for free when using gitlab.com.
* Check the url of the "latest built pdf" on the top of this page, for a quick persistent link to the latest job's created artifacts and adapt it for your repo. For the time being, this is a [documented](https://docs.gitlab.com/ce/user/project/pipelines/job_artifacts.html#downloading-the-latest-artifacts) but without a UI (afaik) feature in gitlab.

## credits
* uses the great [git latexdiff](https://gitlab.com/git-latexdiff/git-latexdiff) project.
* is based on the ```.gitlab-ci.yml``` of [niccokunzmann/ci-latex](https://github.com/niccokunzmann/ci-latex).
* pulls the [niccokunzmann/ci-latex](https://hub.docker.com/r/niccokunzmann/ci-latex) docker image to build the necessary files.

